docker-install role
=========
A brief description of the role goes here :

This role install docker on inventory and add user to docker group

Requirements :pushpin:
------------
Any pre-requisites that may not be covered by Ansible itself : 

- python3
- sudo

Role Variables :wrench:
--------------
```yml
#./default/main.yml
- ansible_user: "{{ ansible_user }}" #remote user used by ansible for docker installation
```

Dependencies :paperclip:
------------
A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles :

None

Vault :lock:
------------
Vault informations .

None

Example Playbook :clapper:
----------------

```yml 
- name : Install docker
  gather_facts: false
  hosts:
    - all
  become: true
  roles:
    - docker-install
```
